package com.r2devops.sisoteuz;

public class HelloWorld {
  public static void main(final String[] args) {
    System.out.println("R2Devops rules the world");
  }

  public int returnFive() {
    return 5;
  }

  public int returnTen() {
    return 10;
  }

  public void notTest() {
  }
}
