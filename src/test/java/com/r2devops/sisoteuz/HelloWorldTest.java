package com.r2devops.sisoteuz;

import org.junit.Test;
import static org.junit.Assert.*;

public class HelloWorldTest {

  @Test
  public void test5() {
    final HelloWorld hw = new HelloWorld();
    assertEquals(5, hw.returnFive());
  }

  @Test
  public void test10() {
    final HelloWorld hw = new HelloWorld();
    assertEquals(10, hw.returnTen());
  }
}
